
/* GAMEBASE KOREA LLC PROPRIETARY INFORMATION
 *
 * This software is supplied under the terms of a license agreement or
 * nondisclosure agreement with Gamebase Korea LLC and may not
 * be copied or disclosed except in accordance with the terms of that
 * agreement.
 *
 *      Copyright (c) 1996-2013 Gamebase Korea LLC.
 *      All Rights Reserved.
 *
 * Gamebase Korea LLC, 
 * http://www.gamebryo.com
 */
/*
============================================
AMMO interface code: This is additional code to use Ammo.js in Haxe.
This code will be added under compiled ammo.js file. 
============================================
*/


var AmmoInterface = {};

AmmoInterface.bodyIdCounter = 0;

AmmoInterface.CollisionFlags = {
    CF_STATIC_OBJECT: 1,
    CF_KINEMATIC_OBJECT: 2,
    CF_NO_CONTACT_RESPONSE: 4,
    CF_CUSTOM_MATERIAL_CALLBACK: 8,
    CF_CHARACTER_OBJECT: 16,
    CF_DISABLE_VISUALIZE_OBJECT: 32,
    CF_DISABLE_SPU_COLLISION_PROCESSING: 64
};

AmmoInterface.ActivationState = {
    ACTIVE_TAG:1,
    ISLAND_SLEEPING:2,
    WANTS_DEACTIVATION:3,
    DISABLE_DEACTIVATION:4,
    DISABLE_SIMULATION:5
};

AmmoInterface.ScalarType = {
    FLOAT: 0,
    DOUBLE: 1,
    INTEGER: 2,
    SHORT: 3,
    FIXEDPOINT88: 4,
    UCHAR: 5
};

AmmoInterface.CollisionFilterGroups = {
    DefaultFilter: 1,
    StaticFilter: 2,
    KinematicFilter: 4,
    DebrisFilter:8,
    SensorTrigger: 16,
    CharacterFilter: 32,
    AllFilter: -1 //all bits sets: DefaultFilter | StaticFilter | KinematicFilter | DebrisFilter | SensorTrigger
};

function Initialize() {
    AmmoInterface.collisionConfiguration = new Ammo.btDefaultCollisionConfiguration(); // every single |new| currently leaks...
    AmmoInterface.dispatcher = new Ammo.btCollisionDispatcher(AmmoInterface.collisionConfiguration);
    AmmoInterface.overlappingPairCache = new Ammo.btDbvtBroadphase();
    AmmoInterface.solver = new Ammo.btSequentialImpulseConstraintSolver();
    AmmoInterface.dynamicsWorld = new Ammo.btDiscreteDynamicsWorld(AmmoInterface.dispatcher, AmmoInterface.overlappingPairCache, AmmoInterface.solver, AmmoInterface.collisionConfiguration);
    AmmoInterface.dynamicsWorld.setGravity(new Ammo.btVector3(0, -10, 0));
    AmmoInterface.dynamicsWorld.getDispatchInfo().set_m_allowedCcdPenetration(0.0001);
    AmmoInterface.haxeBodies = {};
}

function RayCastPosition(start, end, result) {
    var btRayFrom = new Ammo.btVector3(start[0],start[1],start[2]);
    var btRayTo = new Ammo.btVector3(end[0],end[1],end[2]);

    var rayCallback = new Ammo.ClosestRayResultCallback(btRayFrom, btRayTo);
    AmmoInterface.dynamicsWorld.rayTest(btRayFrom, btRayTo, rayCallback);

    var hit = rayCallback.hasHit();
    if (hit) {
        var ht = rayCallback.get_m_hitPointWorld();
        result[0] = ht.getX();
        result[1] = ht.getY();
        result[2] = ht.getZ();
    }

    return hit;
}

function CreateBox(data, info, motionState, mass) {
    var shape = new Ammo.btBoxShape(new Ammo.btVector3(info[0],info[1],info[2]));
    return CreateRigidBody(shape, data, motionState, mass);
}

function CreateCharacter(data, info, mass) {
    var capsule = null;
    var character = null;
    var ghostObject = null;
    var collisionFlags = AmmoInterface.CollisionFlags;
    var collisionFilterGroups = AmmoInterface.CollisionFilterGroups;

    var transform = new Ammo.btTransform();
    transform.setIdentity ();
    transform.setOrigin(new Ammo.btVector3(data[0], data[1], data[2]));

    var characterHeight= info[0];
    var characterWidth = info[1];
    var stepHeight = info[2];

    ghostObject = new Ammo.btPairCachingGhostObject();
    ghostObject.setWorldTransform(transform);
    AmmoInterface.overlappingPairCache.getOverlappingPairCache().setInternalGhostPairCallback(new Ammo.btGhostPairCallback());

    capsule = new Ammo.btSphereShape(characterWidth,characterHeight);
    ghostObject.setCollisionShape(capsule);
    ghostObject.setCollisionFlags(collisionFlags.CF_CHARACTER_OBJECT);

    var localInertia = new Ammo.btVector3(0, 0, 0);
    capsule.calculateLocalInertia(mass,localInertia);

    character = new Ammo.btKinematicCharacterController(ghostObject,capsule,stepHeight);

    AmmoInterface.dynamicsWorld.addCollisionObject(ghostObject, collisionFilterGroups.CharacterFilter,
            collisionFilterGroups.StaticFilter|collisionFilterGroups.DefaultFilter);
    AmmoInterface.dynamicsWorld.addAction(character);

    return StoreCharacterBody(capsule, character, ghostObject, data);
}

function CreateTerrain(data, info, heightField) {
    var heightfieldShape = null;
    var scalarType = AmmoInterface.ScalarType;

    var s_gridSizeX = info[3];
    var s_gridSizeZ = info[4];
    var s_gridHeightScale = info[1];
    var m_minHeight = -1;
    var m_maxHeight = 1;
    var m_upAxis = 1;
    var flipQuadEdges = false;

    var ptr = Ammo.allocate(s_gridSizeX*s_gridSizeZ, 'float', Ammo.ALLOC_NORMAL);
    for (var f = 0, fMax = s_gridSizeX*s_gridSizeZ; f < fMax; f++) {
        Ammo.setValue(ptr+(f<<2), heightField[f], 'float');    
    }
 
    var heightfieldShape = new Ammo.btHeightfieldTerrainShape(
            s_gridSizeX, s_gridSizeZ,
            ptr,
            s_gridHeightScale,
            m_minHeight, m_maxHeight,
            m_upAxis,
            scalarType.FLOAT,
            flipQuadEdges);

    var localScaling = new Ammo.btVector3(info[0],info[1],info[2]);
    heightfieldShape.setLocalScaling(localScaling);
    heightfieldShape.setUseDiamondSubdivision(true);
    return CreateRigidBody(heightfieldShape, data, 1, 0.);
}

function MoveCharacter(id, data) {
    var characterBody = AmmoInterface.haxeBodies[id];
    characterBody.character.setWalkDirection(new Ammo.btVector3(data[0] * data[3], data[1] * data[3], data[2] * data[3]));
}

function RefreshKinematicBodies() {

}

function Step() {
    var bodyData = {};
    AmmoInterface.dynamicsWorld.stepSimulation(1 / 60, 2);
    for (var id in AmmoInterface.haxeBodies) {
        if (id != -1) {
            AmmoInterface.haxeBodies[id].refresh();
            bodyData[id] = AmmoInterface.haxeBodies[id].data;
        }
    }
    return bodyData;
}

function CreateRigidBody(shape, data, motionState, mass) {
    var collisionFlags = AmmoInterface.CollisionFlags;
    var activationState = AmmoInterface.ActivationState;
    var body = null;

    var transform = new Ammo.btTransform();
    transform.setIdentity();
    transform.setOrigin(new Ammo.btVector3(data[0], data[1], data[2]));
    transform.setRotation(new Ammo.btQuaternion(data[3], data[4], data[5], data[6]));

    var isDynamic = (mass > 0.00001);

    var localInertia = new Ammo.btVector3(0, 0, 0);
    if (isDynamic)
        shape.calculateLocalInertia(mass,localInertia);
    else
        mass =  0;
    
    var myMotionState = new Ammo.btDefaultMotionState(transform);
    var rbInfo = new Ammo.btRigidBodyConstructionInfo(mass, myMotionState, shape, localInertia);
    body = new Ammo.btRigidBody(rbInfo);

    /*if (motionState == 2) {
        body.setCollisionFlags(body.getCollisionFlags() | collisionFlags.CF_KINEMATIC_OBJECT);
        body.setActivationState(activationState.DISABLE_DEACTIVATION);
    }*/

    AmmoInterface.dynamicsWorld.addRigidBody(body);

    return StoreDynamicBody(shape, body, data);
}

function StoreDynamicBody(shape, body, data) {
    var bodyId = AmmoInterface.bodyIdCounter++;
    var haxeBody;

    var i, transform = new Ammo.btTransform(), origin, rotation, haxeBodyData;
    haxeBody = {
        shape: shape,
        body: body,
        data: data,
        refresh: function() {
            if (body.mass != 0) {
                body.getMotionState().getWorldTransform(transform); // Retrieve box position & rotation from Ammo
                origin = transform.getOrigin();
                rotation = transform.getRotation();
                data[0] = transform.getOrigin().getX();
                data[1] = transform.getOrigin().getY();
                data[2] = transform.getOrigin().getZ();
                data[3] = transform.getRotation().getX();
                data[4] = transform.getRotation().getY();
                data[5] = transform.getRotation().getZ();
                data[6] = transform.getRotation().getW();
            }
        }
    };
    AmmoInterface.haxeBodies[bodyId] = haxeBody;

    return bodyId;
}

function StoreCharacterBody(shape, character, ghost, data) {
    var bodyId = AmmoInterface.bodyIdCounter++;
    var haxeBody;

    haxeBody = {
        shape: shape,
        ghost: ghost,
        data: data,
        character:character,
        refresh: function() {
            var trans = ghost.getWorldTransform();
            data[0] = trans.getOrigin().getX();
            data[1] = trans.getOrigin().getY();
            data[2] = trans.getOrigin().getZ();
            data[3] = trans.getRotation().getX();
            data[4] = trans.getRotation().getY();
            data[5] = trans.getRotation().getZ();
            data[6] = trans.getRotation().getW();
        }
    };
    AmmoInterface.haxeBodies[bodyId] = haxeBody;
    return bodyId;
}

self.addEventListener('message', function(e) {
  var data = e.data;
  var args = data.args;
  var cmd = data.cmd;
  var ret = {
      cmd: data.cmd,
      callbackId: data.callbackId
  };
  switch (data.cmd){
    case 'Initialize':
      Initialize();
      break;
    case 'Step':
      ret.value = Step();
      break;
    case 'RayCastPosition':
      var hit = RayCastPosition(args[0], args[1], args[2]);
      ret.value = [hit, args[2]];
      break;
    case 'CreateBox':
      ret.value = CreateBox(args[0], args[1], args[2], args[3]);
      break;
    case 'CreateCharacter':
      ret.value = CreateCharacter(args[0], args[1], args[2]);
      break;
    case 'CreateTerrain':
      ret.value = CreateTerrain(args[0], args[1], args[2]);
      break;
    case 'MoveCharacter':
      MoveCharacter(args[0], args[1]);
      break;
  }
  self.postMessage(ret);
}, false);